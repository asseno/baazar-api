package com.bazaar.controller;

import com.bazaar.security.holder.UserDataHolder;
import com.bazaar.security.util.AuthenticatedUserUtil;

public abstract class PrincipalAwareController {
    protected UserDataHolder getPrincipal() {
        return AuthenticatedUserUtil.getPrincipal();
    }
}
