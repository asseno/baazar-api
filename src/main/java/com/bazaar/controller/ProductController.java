package com.bazaar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bazaar.dto.ProductDTO;
import com.bazaar.exception.NotFoundException;
import com.bazaar.mapper.ProductMapper;
import com.bazaar.model.Product;
import com.bazaar.security.util.AuthenticatedUserUtil;
import com.bazaar.service.ProductService;
import com.bazaar.specification.ProductSpecification;

public class ProductController extends PrincipalAwareController {

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductMapper productMapper;

	@GetMapping
	public Page<ProductDTO> getProducts(Pageable pageable) {
		final Page<Product> products = this.productService.findAll(ProductSpecification.of(null), pageable);
		
		return new PageImpl<>(this.productMapper.toDTOs(products.getContent()), pageable, products.getTotalElements());
	}

	@GetMapping("/{id}")
	public ProductDTO getProduct(@PathVariable Long id) {
		return this.productMapper
				.toDTO(this.productService.findOne(id).orElseThrow(() -> new NotFoundException("Product not found")));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ProductDTO> deleteProduct(@PathVariable Long id) {
		Product product = this.productService.findOne(id).orElseThrow(() -> new NotFoundException("Product not found"));
		AuthenticatedUserUtil.canEdit(product);
		
		this.productService.delete(product);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ProductDTO> updateProduct(@PathVariable Long id, @RequestBody ProductDTO dto) {
		Product product = this.productService.findOne(id).orElseThrow(() -> new NotFoundException("Product not found"));
		AuthenticatedUserUtil.canEdit(product);
		
		dto.setId(id);
		this.productService.save(this.productMapper.toEntity(dto));
		return ResponseEntity.ok().build();
	}
	
	@PostMapping
	public ResponseEntity<ProductDTO> saveProduct(@PathVariable Long id, @RequestBody ProductDTO dto) {
		this.productService.save(this.productMapper.toEntity(dto));
		return ResponseEntity.ok().build();
	}
}
