package com.bazaar.exception;

public abstract class BaseException extends RuntimeException {
	private static final long serialVersionUID = -9045016986534930816L;
	private final int statusCode;

    public BaseException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public BaseException(String message, int statusCode, Throwable throwable) {
        super(message, throwable);
        this.statusCode = statusCode;
    }

	public int getStatusCode() {
		return statusCode;
	}
    
}
