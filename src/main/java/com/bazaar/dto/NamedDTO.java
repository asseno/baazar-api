package com.bazaar.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class NamedDTO extends BaseDTO implements Serializable {
	
	private static final long serialVersionUID = -4255260712430428169L;
	
	@Column(name = "name", updatable = true, unique = false)
	private String name;

	public NamedDTO(Long id, String name) {
		super(id);
		this.name = name;
	}
	
	public NamedDTO(Long id) {
		super(id);
	}

	public NamedDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
