package com.bazaar.dto;

import com.bazaar.model.Media.MediaType;

public class ConditionDTO extends BaseDTO {

	private ConditionType type;
	private MediaType operator;
	private String value;

	public ConditionType getType() {
		return type;
	}

	public void setType(ConditionType type) {
		this.type = type;
	}

	public MediaType getOperator() {
		return operator;
	}

	public void setOperator(MediaType operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public enum ConditionType {
		Group, Quantity, Date
	}

	public enum ConditionOperator {
		IS, NOT, MORE_THAN, MORE_THAT_EQUALS, LESS_THAN, LESS_THAN_EQUALS, IN, NOT_IN
	}
}
