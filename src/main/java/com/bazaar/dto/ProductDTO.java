package com.bazaar.dto;

import java.util.List;

public class ProductDTO extends NamedDTO {
	private static final long serialVersionUID = 3475224810700207971L;

	private String description;
	private String shortDescription;
	private String sku;
	private ProductStatusDTO status;
	private List<VisibilityDTO> visibilityRules;
	private CountryDTO originCountry;
	private ArticleDTO article;
	private PackagingDTO packaging;
	private MediaDTO thumbnail;
	private PricingDTO pricing;
	private List<MediaDTO> media;
	private List<ProductDTO> related;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public ProductStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ProductStatusDTO status) {
		this.status = status;
	}

	public List<VisibilityDTO> getVisibilityRules() {
		return visibilityRules;
	}

	public void setVisibilityRules(List<VisibilityDTO> visibilityRules) {
		this.visibilityRules = visibilityRules;
	}

	public CountryDTO getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(CountryDTO originCountry) {
		this.originCountry = originCountry;
	}

	public ArticleDTO getArticle() {
		return article;
	}

	public void setArticle(ArticleDTO article) {
		this.article = article;
	}

	public PackagingDTO getPackaging() {
		return packaging;
	}

	public void setPackaging(PackagingDTO packaging) {
		this.packaging = packaging;
	}

	public MediaDTO getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(MediaDTO thumbnail) {
		this.thumbnail = thumbnail;
	}

	public PricingDTO getPricing() {
		return pricing;
	}

	public void setPricing(PricingDTO pricing) {
		this.pricing = pricing;
	}

	public List<MediaDTO> getMedia() {
		return media;
	}

	public void setMedia(List<MediaDTO> media) {
		this.media = media;
	}

	public List<ProductDTO> getRelated() {
		return related;
	}

	public void setRelated(List<ProductDTO> related) {
		this.related = related;
	}
	
}
