package com.bazaar.dto;

import java.util.List;

public class VisibilityDTO extends BaseDTO {
	
	private Boolean visible;
	private List<ConditionDTO> conditions;

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public List<ConditionDTO> getConditions() {
		return conditions;
	}

	public void setConditions(List<ConditionDTO> conditions) {
		this.conditions = conditions;
	}
	
}
