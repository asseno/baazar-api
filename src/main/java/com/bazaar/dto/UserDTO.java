package com.bazaar.dto;

public class UserDTO extends NamedDTO {

	private static final long serialVersionUID = -6345918245751902653L;
	
	private String email;
	private String phone;
	private MediaDTO avatar;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public MediaDTO getAvatar() {
		return avatar;
	}

	public void setAvatar(MediaDTO avatar) {
		this.avatar = avatar;
	}
	
}
