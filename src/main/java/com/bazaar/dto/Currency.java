package com.bazaar.dto;

public enum Currency {
    EUR,
    USD,
    GBP,
    RSD
}
