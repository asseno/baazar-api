package com.bazaar.dto;

public class PackagingDTO extends BaseDTO {

	private PackagingTypeDTO type;
	private UnitDTO unit;
	private Double quantity;
	private Double weight;

	public PackagingTypeDTO getType() {
		return type;
	}

	public void setType(PackagingTypeDTO type) {
		this.type = type;
	}

	public UnitDTO getUnit() {
		return unit;
	}

	public void setUnit(UnitDTO unit) {
		this.unit = unit;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

}
