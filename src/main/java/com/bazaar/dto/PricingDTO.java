package com.bazaar.dto;

import java.util.List;

public class PricingDTO extends BaseDTO {
	private Double price;
	private Currency currency;
	private TaxClassDTO taxClass;
	private List<PricingRuleDTO> rules;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public TaxClassDTO getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(TaxClassDTO taxClass) {
		this.taxClass = taxClass;
	}

	public List<PricingRuleDTO> getRules() {
		return rules;
	}

	public void setRules(List<PricingRuleDTO> rules) {
		this.rules = rules;
	}

}
