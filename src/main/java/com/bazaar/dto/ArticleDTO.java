package com.bazaar.dto;

import java.util.List;

public class ArticleDTO extends NamedDTO {

	private static final long serialVersionUID = -547915085915125488L;
	
	private MediaDTO image;
	private UnitDTO unit;
	private List<CategoryDTO> categories;

	public ArticleDTO() {
	}

	public ArticleDTO(Long id) {
		super(id);
	}

	public MediaDTO getImage() {
		return image;
	}

	public void setImage(MediaDTO image) {
		this.image = image;
	}

	public UnitDTO getUnit() {
		return unit;
	}

	public void setUnit(UnitDTO unit) {
		this.unit = unit;
	}

	public List<CategoryDTO> getCategories() {
		return categories;
	}

	public void setCategories(List<CategoryDTO> categories) {
		this.categories = categories;
	}

}
