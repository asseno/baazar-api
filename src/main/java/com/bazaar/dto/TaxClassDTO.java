package com.bazaar.dto;

public class TaxClassDTO extends NamedDTO {

	private static final long serialVersionUID = -857539775933894268L;
	
	private Double rate;

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}
