package com.bazaar.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.bazaar.model.Product;

public class ProductSpecification implements Specification<Product> {

    @SuppressWarnings("unused")
	private final Product product;
    
    public ProductSpecification(Product product) {
		super();
		this.product = product;
	}

	@Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		final List<Predicate> predicates = new ArrayList<>();
        return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
    }

    public static ProductSpecification of(Product product) {
        return new ProductSpecification(product);
    }
}
