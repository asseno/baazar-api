package com.bazaar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@ComponentScan("com.bazaar")
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "com.bazaar.repository")
@EntityScan(basePackages = { "com.bazaar.model" })
@EnableWebMvc
@EnableSpringDataWebSupport
@SpringBootApplication
public class BazaarApp {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BazaarApp.class, args);
	}

	@Bean
	public MacSigner signer(@Value("${security.jwt.secret.key:secret}") String jwtSecretKey) {
		return new MacSigner(jwtSecretKey);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
