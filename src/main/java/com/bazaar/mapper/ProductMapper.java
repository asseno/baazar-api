package com.bazaar.mapper;


import java.util.List;

import org.mapstruct.Mapper;

import com.bazaar.dto.ProductDTO;
import com.bazaar.model.Product;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductDTO toDTO(Product entity);
    Product toEntity(ProductDTO dto);
    List<ProductDTO> toDTOs(List<Product> list);
    List<Product> toEntities(List<ProductDTO> list);
}
