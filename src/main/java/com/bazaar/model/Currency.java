package com.bazaar.model;

public enum Currency {
    EUR,
    USD,
    GBP,
    RSD
}
