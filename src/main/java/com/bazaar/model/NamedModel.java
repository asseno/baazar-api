package com.bazaar.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class NamedModel extends BaseModel implements Serializable {
	
	private static final long serialVersionUID = -4255260712430428169L;
	
	@Column(name = "name", updatable = true, unique = false)
	private String name;

	public NamedModel(Long id, String name) {
		super(id);
		this.name = name;
	}
	
	public NamedModel(Long id) {
		super(id);
	}

	public NamedModel() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
