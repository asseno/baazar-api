package com.bazaar.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pricing")
public class Pricing extends BaseModel {

	@Column(name = "price")
	private Double price;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@ManyToOne
	@JoinColumn(name = "tax_class_id")
	private TaxClass taxClass;

	@OneToMany
	private List<PricingRule> rules;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public TaxClass getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(TaxClass taxClass) {
		this.taxClass = taxClass;
	}

	public List<PricingRule> getRules() {
		return rules;
	}

	public void setRules(List<PricingRule> rules) {
		this.rules = rules;
	}

}
