package com.bazaar.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product extends NamedModel {

	private static final long serialVersionUID = -8367875595169067349L;

	@Column(name = "description")
	private String description;
	
	@Column(name = "short_description")
	private String shortDescription;
	
	@Column(name = "sku")
	private String sku;
	
	@ManyToOne
	@JoinColumn(name = "product_status_id")
	private ProductStatus status;
	
	@OneToMany
	private List<Visibility> visibilityRules;
	
	@ManyToOne
	@JoinColumn(name = "origin_country_id")
	private Country originCountry;
	
	@ManyToOne
	@JoinColumn(name = "article_id")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "packaging_id")
	private Packaging packaging;
	
	@OneToOne
	@JoinColumn(name = "thumbnail_media_id")
	private Media thumbnail;
	
	@ManyToOne
	@JoinColumn(name = "pricing_id")
	private Pricing pricing;
	
	@ManyToMany
	private List<Media> media;
	
	@ManyToMany
	private List<Product> related;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public ProductStatus getStatus() {
		return status;
	}

	public void setStatus(ProductStatus status) {
		this.status = status;
	}

	public List<Visibility> getVisibilityRules() {
		return visibilityRules;
	}

	public void setVisibilityRules(List<Visibility> visibilityRules) {
		this.visibilityRules = visibilityRules;
	}

	public Country getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(Country originCountry) {
		this.originCountry = originCountry;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Packaging getPackaging() {
		return packaging;
	}

	public void setPackaging(Packaging packaging) {
		this.packaging = packaging;
	}

	public Media getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(Media thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Pricing getPricing() {
		return pricing;
	}

	public void setPricing(Pricing pricing) {
		this.pricing = pricing;
	}

	public List<Media> getMedia() {
		return media;
	}

	public void setMedia(List<Media> media) {
		this.media = media;
	}

	public List<Product> getRelated() {
		return related;
	}

	public void setRelated(List<Product> related) {
		this.related = related;
	}
	
}
