package com.bazaar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tax_class")
public class TaxClass extends NamedModel {

	private static final long serialVersionUID = 697834151818130116L;

	@Column(name = "rate")
	private Double rate;

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}
