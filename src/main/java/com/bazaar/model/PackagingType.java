package com.bazaar.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "packaging_type")
public class PackagingType extends NamedModel {

	private static final long serialVersionUID = -316991774365082578L;

}
