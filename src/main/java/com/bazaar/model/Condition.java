package com.bazaar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.bazaar.model.Media.MediaType;

@Entity
@Table(name = "condition")
public class Condition extends BaseModel {

	@Column(name = "condition_type")
	@Enumerated(EnumType.STRING)
	private ConditionType type;

	@Column(name = "condition_operator")
	@Enumerated(EnumType.STRING)
	private MediaType operator;

	@Column(name = "value")
	private String value;

	public ConditionType getType() {
		return type;
	}

	public void setType(ConditionType type) {
		this.type = type;
	}

	public MediaType getOperator() {
		return operator;
	}

	public void setOperator(MediaType operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public enum ConditionType {
		Group, Quantity, Date
	}

	public enum ConditionOperator {
		IS, NOT, MORE_THAN, MORE_THAT_EQUALS, LESS_THAN, LESS_THAN_EQUALS, IN, NOT_IN
	}
}
