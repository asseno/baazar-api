package com.bazaar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "packaging")
public class Packaging extends BaseModel {

	@ManyToOne
	@JoinColumn(name = "packaging_type_id")
	private PackagingType type;
	
	@ManyToOne
	@JoinColumn(name = "unit_id")
	private Unit unit;
	
	@Column(name = "quantity")
	private Double quantity;

	@Column(name = "weight")
	private Double weight;

	public PackagingType getType() {
		return type;
	}

	public void setType(PackagingType type) {
		this.type = type;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

}
