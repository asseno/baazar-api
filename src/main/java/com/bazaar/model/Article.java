package com.bazaar.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "article")
public class Article extends NamedModel {

	private static final long serialVersionUID = -4007772927973057675L;

	@OneToOne
	@JoinColumn(name = "image_media_id")
	private Media image;

	@ManyToOne
	@JoinColumn(name = "unit_id")
	private Unit unit;

	@ManyToMany
	private List<Category> categories;

	public Article() {
	}

	public Article(Long id) {
		super(id);
	}

	public Media getImage() {
		return image;
	}

	public void setImage(Media image) {
		this.image = image;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
