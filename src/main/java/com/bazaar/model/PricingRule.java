package com.bazaar.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pricing_rule")
public class PricingRule extends BaseModel {

	@Column(name = "price")
	private Double price;

	@Column(name = "visible")
	private Boolean visible;

	@Column(name = "price_type")
	@Enumerated(EnumType.STRING)
	private PriceType priceType;

	@Column(name = "currency")
	@Enumerated(EnumType.STRING)
	private Currency currency;

	@ManyToOne
	@JoinColumn(name = "tax_class_id")
	private TaxClass taxClass;

	@OneToMany
	private List<Condition> conditions;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public PriceType getPriceType() {
		return priceType;
	}

	public void setPriceType(PriceType priceType) {
		this.priceType = priceType;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public TaxClass getTaxClass() {
		return taxClass;
	}

	public void setTaxClass(TaxClass taxClass) {
		this.taxClass = taxClass;
	}

	public List<Condition> getConditions() {
		return conditions;
	}

	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}

	public enum PriceType {
		Absolute, Discount
	}

}
