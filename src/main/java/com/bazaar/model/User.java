package com.bazaar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User extends NamedModel {

	private static final long serialVersionUID = 4035377029904114486L;

	@Column(name = "email", updatable = true, unique = true)
	private String email;
	
	@Column(name = "phone", updatable = true, unique = true)
	private String phone;
	
	@OneToOne
	@JoinColumn(name = "avatar_media_id")
	private Media avatar;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Media getAvatar() {
		return avatar;
	}

	public void setAvatar(Media avatar) {
		this.avatar = avatar;
	}
	
}
