package com.bazaar.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.SignatureVerifier;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.bazaar.security.holder.UserDataHolder;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final ObjectMapper objectMapper;
    private final SignatureVerifier verifier;
    
    @Autowired
    public JwtAuthenticationProvider(SignatureVerifier verifier, ObjectMapper objectMapper) {
        this.verifier = verifier;
        this.objectMapper = objectMapper;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            final Jwt jwt = JwtHelper.decodeAndVerify((String) authentication.getPrincipal(), verifier);
            final UserDataHolder userDataHolder = objectMapper.readValue(jwt.getClaims(), UserDataHolder.class);
            return new JwtAuthentication(userDataHolder);
        } catch (Exception e) {
            log.warn("Unable to parse user data from JWT token.");
            throw new BadCredentialsException("Invalid token.");
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return PreAuthenticatedAuthenticationToken.class.isAssignableFrom(aClass);
    }


}
