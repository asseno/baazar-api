package com.bazaar.security.util;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.bazaar.exception.ForbiddenException;
import com.bazaar.model.Product;
import com.bazaar.model.User;
import com.bazaar.security.holder.UserDataHolder;

public class AuthenticatedUserUtil {

	public static final boolean isAnonymousUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth == null || !auth.isAuthenticated()
				|| auth.getClass().isAssignableFrom(AnonymousAuthenticationToken.class);
	}

	public static final UserDataHolder getPrincipal() {
		if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDataHolder) {
			return (UserDataHolder) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} else {
			throw new ForbiddenException("");
		}
	}

	public static final User getUser() {
		return getPrincipal() != null ? getPrincipal().getUser() : null;
	}


	public static final void checkView(Object objectToCheck) {
		
	}

	public static final void checkEdit(Object objectToCheck) {
		
	}

	// Product
	public static final boolean canView(Product product) {
		return true;
	}

	public static final boolean canEdit(Product product) {
		return true;
	}

}
