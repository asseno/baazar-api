package com.bazaar.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.bazaar.security.holder.UserDataHolder;

public class JwtAuthentication implements Authentication {
	private static final long serialVersionUID = 6643445519224688209L;
	
	private final UserDataHolder userDataHolder;
    private final Collection<GrantedAuthority> authorities;

    public JwtAuthentication(UserDataHolder userDataHolder, Collection<GrantedAuthority> authorities) {
        this.userDataHolder = userDataHolder;
        this.authorities = authorities;
    }

    public JwtAuthentication(UserDataHolder userDataHolder) {
        this(userDataHolder, null);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return userDataHolder;
    }

    @Override
    public Object getPrincipal() {
        return userDataHolder;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return Long.toString(userDataHolder.getUser().getId());
    }
}
