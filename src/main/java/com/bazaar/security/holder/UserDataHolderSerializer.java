package com.bazaar.security.holder;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class UserDataHolderSerializer extends JsonSerializer<UserDataHolder> {
    @Override
    public void serialize(UserDataHolder userDataHolder, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("user_id", userDataHolder.getUser().getId());
        jsonGenerator.writeEndObject();
    }
}
