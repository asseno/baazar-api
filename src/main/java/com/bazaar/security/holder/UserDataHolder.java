package com.bazaar.security.holder;


import com.bazaar.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = UserDataHolderSerializer.class)
public class UserDataHolder {
	
    @JsonProperty(required = true, value = "user_id")
    private final User user;

    public UserDataHolder(@JsonProperty(required = true, value = "user_id") Long userId,
    		@JsonProperty(required = false, value = "user_type") Long userType,
    		@JsonProperty(required = false, value = "first_name") String firstName,
    		@JsonProperty(required = false, value = "last_name") String lastName,
    		@JsonProperty(required = false, value = "profile_picture_url") String profilePictureUrl) {
        user = new User();
        user.setId(userId);
    }

    public User getUser() {
        return user;
    }
}
