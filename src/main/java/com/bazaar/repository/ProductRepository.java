package com.bazaar.repository;

import com.bazaar.model.Product;

public interface ProductRepository extends FilterableJpaRepository<Product, Long> {

}
