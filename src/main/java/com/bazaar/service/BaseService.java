package com.bazaar.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public abstract class BaseService<R extends JpaRepository<T, ID>, T, ID extends Serializable> {

    public Optional<T> findOne(ID id) {
        return Optional.ofNullable(getRepository().findOne(id));
    }

    public T save(T entity) {
        return getRepository().save(entity);
    }

    public List<T> save(Iterable<T> entities) {
        return getRepository().save(entities);
    }

    public T saveAndFlush(T entity) {
        return getRepository().saveAndFlush(entity);
    }

    public List<T> findAll() {
        return getRepository().findAll();
    }
    
    public List<T> findAll(Iterable<ID> ids) {
        return getRepository().findAll(ids);
    }

    public Page<T> findAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    public void delete(T entity) {
        getRepository().delete(entity);
    }

    public void delete(ID id) {
        getRepository().delete(id);
    }

    public void delete(Iterable<T> ids) {
        getRepository().delete(ids);
    }

    public boolean exists(ID id) {
        return getRepository().exists(id);
    }

    protected abstract R getRepository();

}
