package com.bazaar.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.bazaar.repository.FilterableJpaRepository;

public abstract class FilterableService<R extends FilterableJpaRepository<T, ID>, T, ID extends Serializable>
		extends BaseService<R, T, ID> {

	public Optional<T> findOne(Specification<T> spec) {
		return Optional.ofNullable(getRepository().findOne(spec));
	}
	
	public List<T> findAll(Specification<T> spec) {
		return getRepository().findAll(spec);
	}

	public Page<T> findAll(Specification<T> spec, Pageable pageable) {
		return getRepository().findAll(spec, pageable);
	}

	public List<T> findAll(Specification<T> spec, Sort sort) {
		return getRepository().findAll(spec, sort);
	}

	public long count(Specification<T> spec) {
		return getRepository().count(spec);
	}

}
