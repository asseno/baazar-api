package com.bazaar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bazaar.model.Product;
import com.bazaar.repository.ProductRepository;

@Service
public class ProductService extends FilterableService<ProductRepository, Product, Long> {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	protected ProductRepository getRepository() {
		return this.productRepository;
	}

}
