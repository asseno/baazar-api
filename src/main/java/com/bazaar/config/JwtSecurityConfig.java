package com.bazaar.config;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.web.filter.GenericFilterBean;

import com.bazaar.security.JwtAuthenticationProvider;
import com.bazaar.security.JwtTokenPreAuthenticatedProcessingFilter;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class JwtSecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${security.jwt.secret.key:secret}")
    private String JWTSecretKey;

    private final ObjectMapper objectMapper;

    @Autowired
    public JwtSecurityConfig(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Bean
    public JwtAuthenticationProvider userDataAuthenticationProvider() {
        return new JwtAuthenticationProvider(new MacSigner(this.JWTSecretKey), this.objectMapper);
    }

    @Bean
    public JwtTokenPreAuthenticatedProcessingFilter serviceRequestHeaderAuthenticationFilter() {
        ProviderManager providerManager = new ProviderManager(Collections.singletonList(userDataAuthenticationProvider()));
        JwtTokenPreAuthenticatedProcessingFilter jwtTokenPreAuthenticatedProcessingFilter = new JwtTokenPreAuthenticatedProcessingFilter();
        jwtTokenPreAuthenticatedProcessingFilter.setAuthenticationManager(providerManager);
        return jwtTokenPreAuthenticatedProcessingFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .requestCache().requestCache(new NullRequestCache())
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable()
                .addFilterBefore(new CorsAwareAuthenticationFilter(), JwtTokenPreAuthenticatedProcessingFilter.class)
                .addFilter(serviceRequestHeaderAuthenticationFilter())
                .logout()
                // force an arbitrary logout url
                .logoutUrl("/sklj");
    }

    public class CorsAwareAuthenticationFilter extends GenericFilterBean {
        static final String ORIGIN = "Origin";

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                throws IOException, ServletException {
            if (((HttpServletRequest) request).getHeader(ORIGIN) != null) {
                String origin = ((HttpServletRequest) request).getHeader(ORIGIN);
                ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", origin);
                ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods",
                        "GET, POST, PUT, DELETE");
                ((HttpServletResponse) response).addHeader("Access-Control-Allow-Credentials", "true");
                ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers",
                        ((HttpServletRequest) request).getHeader("Access-Control-Request-Headers"));
            }
            if ("OPTIONS".equals(((HttpServletRequest) request).getMethod())) {
                try {
                    response.getWriter().print("OK");
                    response.getWriter().flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                chain.doFilter(request, response);
            }

        }
    }
}
